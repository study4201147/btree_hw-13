/**
* Code generation. Don't modify! 
 */
namespace AIModule
{
    public static class BlackboardAPI
    {
        public const ushort Character = 1; // IAtomicObject
        public const ushort Barn = 2; // IAtomicObject
        public const ushort Resource = 3; // IAtomicObject
        public const ushort ResourceService = 4; // ResourceService
        public const ushort Target = 5; // Vector3
        public const ushort StoppingDistance = 6; // float
        public const ushort DialogBarnIsFull = 7; // GameObject
        public const ushort DialogNoTrees = 8; // GameObject
    }
}
