using System;
using AIModule;
using Atomic.Objects;
using UnityEngine;

namespace Game.Engine
{
    [Serializable]
    public sealed class BTNode_UnloadResources : BTNode
    {
        public override string Name => "Unload Resources";

        [SerializeField, BlackboardKey]
        private ushort Character;

        [SerializeField, BlackboardKey]
        private ushort TargetStorage;
        
        protected override BTState OnUpdate(IBlackboard blackboard, float deltaTime)
        {
            if (!(blackboard.TryGetObject(Character, out IAtomicObject characterObject) &&
                  blackboard.TryGetObject(TargetStorage, out IAtomicObject barn)))
            {
                return BTState.FAILURE;
            }

            var targetStorage = barn.Get<ResourceStorage>(ObjectAPI.ResourceStorage);

            if (targetStorage.IsFull())
            {
                return BTState.FAILURE;
            }
            
            var characterStorage = characterObject.Get<ResourceStorage>(ObjectAPI.ResourceStorage);

            if (characterStorage.IsEmpty())
            {
                return BTState.FAILURE;
            }

            int resourcesToExtract = characterStorage.Current > targetStorage.FreeSlots ?
                targetStorage.FreeSlots : characterStorage.Current;

            characterStorage.ExtractResources(resourcesToExtract);
            targetStorage.PutResources(resourcesToExtract);

            return BTState.SUCCESS;
        }
    }
}