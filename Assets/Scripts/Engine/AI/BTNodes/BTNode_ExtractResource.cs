using System;
using AIModule;
using Atomic.Elements;
using Atomic.Extensions;
using Atomic.Objects;
using UnityEngine;

namespace Game.Engine
{
    [Serializable]
    public sealed class BTNode_ExtractResource : BTNode
    {
        public override string Name => "Extract Resource";

        [SerializeField, BlackboardKey]
        private ushort Character;
        
        [SerializeField, BlackboardKey]
        private ushort Resource;

        [SerializeField, BlackboardKey]
        private ushort MinDistance;

        protected override BTState OnUpdate(IBlackboard blackboard, float deltaTime)
        {
            if (!blackboard.TryGetObject(Character, out IAtomicObject characterObject))
            {
                return BTState.FAILURE;
            }

            if (characterObject.Get<ResourceStorage>(ObjectAPI.ResourceStorage).IsFull())
            {
                return BTState.SUCCESS;
            }

            if (!(blackboard.TryGetObject(Resource, out IAtomicObject tree) &&
                  blackboard.TryGetFloat(MinDistance, out float distance)))
            {
                return BTState.FAILURE;
            }

            if (tree.Get<ResourceStorage>(ObjectAPI.ResourceStorage).IsEmpty())
            {
                return BTState.SUCCESS;
            }

            var treeTransform = tree.Get<Transform>(ObjectAPI.Transform);
            var characterTransform = characterObject.Get<Transform>(ObjectAPI.Transform);

            Vector3 direction = treeTransform.position - characterTransform.position;
            if (direction.sqrMagnitude > distance * distance)
            {
                return BTState.FAILURE;
            }
            
            characterObject.Get<IAtomicVariable<Vector3>>(ObjectAPI.LookDirection).Value = direction.normalized;
            characterObject.InvokeAction(ObjectAPI.GatherRequest);
            
            return BTState.RUNNING;
        }
    }
}