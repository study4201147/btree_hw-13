using System;
using AIModule;
using Atomic.Elements;
using Atomic.Extensions;
using Atomic.Objects;
using UnityEngine;

namespace Game.Engine
{
    [Serializable]
    public sealed class BTNode_MoveToTarget : BTNode
    {
        public override string Name => "Move To Target";

        [SerializeField, BlackboardKey]
        private ushort Character;

        [SerializeField, BlackboardKey]
        private ushort Target;

        [SerializeField, BlackboardKey]
        private ushort StoppingDistance;

        protected override BTState OnUpdate(IBlackboard blackboard, float deltaTime)
        {
            if (!(blackboard.TryGetObject(Character, out IAtomicObject characterObject) &&
                  blackboard.TryGetObject(Target, out IAtomicObject target) &&    
                  blackboard.TryGetFloat(StoppingDistance, out float stoppingDistance)))
            {
                return BTState.FAILURE;
            }

            if (target.Is(ObjectType.Resource) && !target.Get<IAtomicValue<bool>>(ObjectAPI.IsActive).Value)
            {
                return BTState.FAILURE;
            }

            Vector3 targetPosition = target.Get<Transform>(ObjectAPI.Transform).position;
            
            Vector3 direction = targetPosition - characterObject.Get<Transform>(ObjectAPI.Transform).position;
            if (direction.sqrMagnitude <= stoppingDistance * stoppingDistance)
            {
                return BTState.SUCCESS;
            }

            characterObject.InvokeAction(ObjectAPI.MoveStepRequest, direction.normalized);

            return BTState.RUNNING;
        }
    }
}