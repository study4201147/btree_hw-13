using System;
using AIModule;
using Atomic.Objects;
using UnityEngine;

namespace Game.Engine
{
    [Serializable]
    public sealed class BTNode_FindResource : BTNode
    {
        public override string Name => "Find Resource";

        [SerializeField, BlackboardKey]
        private ushort Character;

        [SerializeField, BlackboardKey]
        private ushort ResourceService;
        
        [SerializeField, BlackboardKey]
        private ushort TargetResource;

        protected override BTState OnUpdate(IBlackboard blackboard, float deltaTime)
        {
            if (!(blackboard.TryGetObject(Character, out IAtomicObject characterObject) &&
                  blackboard.TryGetObject(ResourceService, out object resourceServiceObj)))
            {
                return BTState.FAILURE;
            }

            if (resourceServiceObj is not ResourceService resourceService)
            {
                return BTState.FAILURE;
            }

            var characterTransform = characterObject.Get<Transform>(ObjectAPI.Transform);
            bool found = resourceService.FindClosestResource(characterTransform.position, out IAtomicObject tree);
            if (!found)
            {
                return BTState.FAILURE;
            }

            blackboard.SetObject(TargetResource, tree);
            
            return BTState.SUCCESS;
        }
    }
}