using AIModule;
using Atomic.Objects;
using Game.Engine;
using UnityEngine;

namespace Engine.AI.Conditions
{
    [CreateAssetMenu(
        fileName = "AICondition_ResourceStorageIsFull", 
        menuName = "AI/Conditions/AICondition_ResourceStorageIsFull"
        )]
    public class AICondition_ResourceStorageIsFull : AICondition
    {
        [SerializeField, BlackboardKey] 
        private ushort Entity;
        
        public override bool Check(IBlackboard blackboard)
        {
            if (!blackboard.TryGetObject(Entity, out IAtomicObject entity))
            {
                return false;
            }

            var resourceStorage = entity.Get<ResourceStorage>(ObjectAPI.ResourceStorage);
            return resourceStorage.IsFull();
        }
    }
}