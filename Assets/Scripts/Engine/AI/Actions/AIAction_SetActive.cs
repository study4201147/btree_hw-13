using AIModule;
using UnityEngine;

namespace Engine.AI.Actions
{
    [CreateAssetMenu(
        fileName = "AIAction_SetActive", 
        menuName = "AI/Actions/AIAction_SetActive"
    )]
    public class AIAction_SetActive : AIAction
    {
        [SerializeField, BlackboardKey] 
        private ushort Panel;

        [SerializeField] 
        private bool IsActive;
        
        public override void Perform(IBlackboard blackboard)
        {
            if (!blackboard.TryGetObject(Panel, out GameObject go))
            {
                return;
            }
            
            go.SetActive(IsActive);
        }
    }
}